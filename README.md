# doc.wetbat.com



## How to use the Platform

Now I am going to describe de flow of the application and how the Backend works. First of all, access the Home Page in https://portal.wetbat.paradoxo.dev.br. Fill the form to find an available airplane based on the parameters you passed.

![WetBat Home Page](./images/wetbat-home.png)

You will recieve a message indicating the your flyght was found. Now you have to procede and click in Quote button to create your first quote.

![WetBat Home Page](./images/wetbat-home-flyght-found.png)

Fill the form with your personal contact info to finish the quote. After that, you can go to quotes page to see all your quotes.

![WetBat Home Page](./images/wetbat-home-flyght-found-fill-form.png)

For each quote, you have to possibilities:
- Convert that quote into a service
- Get the price of that quote.

If you choose get the price, you will call a domain service resposible for generate a price based on the locations of the airports.

![WetBat Quotes Page get price](./images/wetbat-quotes-get-price.png)

![WetBat Quotes Page domain service](./images/wetbat-quotes-get-price-domain-service.png)

If you choose convert that quote into a service, a domain event will be trigged and an invoice will be created with status ```PENDING```

![WetBat Quotes Page domain service](./images/wetbat-quotes-get-price-domain-event.png)

![WetBat Quotes Page domain service](./images/wetbat-quotes-get-price-domain-listener.png)

If you move to Invoces Page, you will have the possility of confirm or cancel that service. This process is made manually because there isnt any integration with Payment Gateway.

![WetBat Invoices Page](./images/wetbat-invoices.png)


## BI

All of the business questions can be answered by BI. You can access it on https://bi.wetbat.paradoxo.dev.br. The login is

```bash
login=alexandre.diniz@wetbat.com
password=23,ZEx2C8"iw
```

For simplicity, there are only two questions answered in BI.

- Get all airports
- Get all invoices

![WetBat BI](./images/wetbat-bi.png)

## API

The API was developed using NestJS, an Javascript framework. the API was desinged using Clean Architecture and concepts of DDD

![WetBat API Desing](./images/wetbat-api-desing.png)

You can read the api docuentation in https://api.wetbat.paradoxo.dev.br/api/docs

![WetBat API Docs](./images/wetbat-api-docs.png)


## Instalation & Configuration

### API

This application runs on a docker container. If you do not have it installed yet, [click here](https://docs.docker.com/engine/install/ubuntu/). You have to create a .env file with at least those pair key/values:

```bash
DATABASE_HOST=host.docker.internal # your db host
DATABASE_PORT=3312 # your db port
DATABASE_USER=root # your db user
DATABASE_PASSWORD=123456 # your db password
DATABASE_TYPE=mysql # your db database type
DATABASE_NAME=wetbat # your db database name
```

After that, run the follwing command:
```bash
docker compose up -d --build
```

The application will start up on ```http://your-host:3000```

You can read the Api Docs in ```http://localhost:3000/api/docs``` if you are in desenv environment. If you want to access and read the api and docs in production, follow these liks: [API](https://api.wetbat.paradoxo.dev.br) & [DOCS](https://api.wetbat.paradoxo.dev.br/api/docs)

#### Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Platform

Create a ```.env.local``` with this content:

```bash
NEXT_PUBLIC_API_URI=https://api.wetbat.paradoxo.dev.br
```

Remember to change the IP to your host IP. To run this project in a container, run the following command:

```bash
docker compose up -d
```

This will start up a container running on port ```3001```. If you want to access the production platform, click on this [link](https://portal.wetbat.paradoxo.dev.br)

### Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

## DataBase

This database runs on a docker container. First, you need to make the script file:

```bash
chmod +x setup.sh
```

After that, you have to build and run the docker container with this command:

```bash
./setup.sh
```

This script will build and run the docker db container with the ```db.sql``` inside the ```tmp``` folder. This step is required to execute the migration script in order to create all database tables. When the container is running, run the following command:

```bash
docker exec -it wetbat-db mysql -u root -p wetbat < /tmp/db.sql
```

Provided the password: ```123456```. Don't worry, this is not an offial project, so you do not need to worry about protect this passowrd.

## BI
To start up the BI, just execute the following command and then access http://localhost:3002. 

```bash
docker compose up -d --build
```

To access the BI in production, [click here](https://bi.wetbat.paradoxo.dev.br)
